package com.nashmaniac.sunshine;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import com.nashmaniac.sunshine.data.WeatherContract;
import com.nashmaniac.sunshine.data.WeatherContract.LocationEntry;
import com.nashmaniac.sunshine.data.WeatherContract.WeatherEntry;
import com.nashmaniac.sunshine.data.WeatherDbHelper;

/**
 * Created by nashmaniac on 6/28/16.
 */
public class SampleTest extends AndroidTestCase {
    public void deleteDatabase(){
        mContext.deleteDatabase(WeatherDbHelper.DATABASE_NAME);
    }
    @Override
    protected void setUp() throws Exception {
        deleteDatabase();
    }
    public ContentValues getLocationContentValues(){
        ContentValues c = new ContentValues();
        c.put(LocationEntry.COLUMN_QUERY, "london");
        c.put(LocationEntry.COLUMN_PLACE_NAME, "London, UK");
        c.put(LocationEntry.COLUMN_LATITUDE,35.67 );
        c.put(LocationEntry.COLUMN_LONGITUDE, 45.67);
        return c;
    }
    static final long TEST_DATE = 1419033600L;
    public ContentValues getWeatherValues(Long locationId){
        ContentValues c = new ContentValues();
        c.put(WeatherEntry.COLUMN_LOC_KEY, locationId);
        c.put(WeatherEntry.COLUMN_DATE, TEST_DATE);
        c.put(WeatherEntry.COLUMN_DESCRIPTION, "nothing serious");
        c.put(WeatherEntry.COLUMN_HUMIDITY, 78.4);
        c.put(WeatherEntry.COLUMN_MAX_TEMP, 94);
        c.put(WeatherEntry.COLUMN_MIN_TEMP, 75);
        c.put(WeatherEntry.COLUMN_PRESSURE, 75);
        c.put(WeatherEntry.COLUMN_WEATHER_ID, 100);
        return c;
    }


    public void CreateDB() throws Exception{
        SQLiteDatabase sqLiteDatabase = new WeatherDbHelper(this.mContext).getWritableDatabase();
        assertEquals(true, sqLiteDatabase.isOpen());
        Cursor c = sqLiteDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        int columnIndex = c.getColumnIndex("name");
        c.moveToFirst();
        while (c.moveToNext()){
            Log.v("table: ", c.getString(columnIndex));
        }
    }
    public SQLiteDatabase getDBReferences(){
        return new WeatherDbHelper(this.mContext).getWritableDatabase();
    }

    public void testInsertLocationToLocationTable() throws Exception{
        CreateDB();
        SQLiteDatabase sqLiteDatabase = getDBReferences();
        ContentValues c = getLocationContentValues();
        Long locationRowId = sqLiteDatabase.insert(LocationEntry.TABLE_NAME, null, c);
        Log.v("locationRow", Long.toString(locationRowId));
        Cursor co = sqLiteDatabase.rawQuery("select name, query from Location", null);
        co.moveToFirst();
        int columnIndex = co.getColumnIndex(LocationEntry.COLUMN_PLACE_NAME);
        int columnIndex2 = co.getColumnIndex(LocationEntry.COLUMN_QUERY);
        do{
            Log.v("name:", co.getString(columnIndex));
            Log.v("query:", co.getString(columnIndex2));
            co.moveToNext();
        }while(co.moveToNext());
        co.close();
        sqLiteDatabase.close();
    }
    public void testInsertWeatherToWeatherTable() throws Exception{
        CreateDB();
        SQLiteDatabase sqLiteDatabase = getDBReferences();
        ContentValues c = getLocationContentValues();
        Long locationRowId = sqLiteDatabase.insert(LocationEntry.TABLE_NAME, null, c);
        Log.v("locationRow", Long.toString(locationRowId));
        ContentValues w = getWeatherValues(locationRowId);
        Long weatherLocationID = sqLiteDatabase.insert(WeatherEntry.TABLE_NAME, null, w);
        Cursor co = sqLiteDatabase.query(WeatherEntry.TABLE_NAME, new String[]{"max", "min"}, null, null, null, null, null);
        int columnIndex = co.getColumnIndex(WeatherEntry.COLUMN_MAX_TEMP);
        int columnIndex2 = co.getColumnIndex(WeatherEntry.COLUMN_MIN_TEMP);
        co.moveToFirst();
        do{
            Log.v("max:", Double.toString(co.getDouble(columnIndex)));
            Log.v("min:", Double.toString(co.getDouble(columnIndex2)));
            co.moveToNext();
        }while (co.moveToNext());
        co.close();
        sqLiteDatabase.close();
    }
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        deleteDatabase();
    }
}
