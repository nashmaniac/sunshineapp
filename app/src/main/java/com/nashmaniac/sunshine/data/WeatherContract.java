package com.nashmaniac.sunshine.data;

import android.provider.BaseColumns;

/**
 * Created by nashmaniac on 6/28/16.
 */
public class WeatherContract {
    public static final class LocationEntry implements BaseColumns{
        public static final String TABLE_NAME = "Location";
        public static final String COLUMN_PLACE_NAME = "name";
        public static final String COLUMN_QUERY= "query";
        public static final String COLUMN_LATITUDE= "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";
    }

    public static final class WeatherEntry implements BaseColumns{
        public static final String TABLE_NAME = "Weather";
        public static final String COLUMN_LOC_KEY = "location_id";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_WEATHER_ID = "weather_id";
        public static final String COLUMN_MAX_TEMP = "max";
        public static final String COLUMN_MIN_TEMP = "min";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_PRESSURE = "pressure";
        public static final String COLUMN_HUMIDITY = "humidity";
    }
}
