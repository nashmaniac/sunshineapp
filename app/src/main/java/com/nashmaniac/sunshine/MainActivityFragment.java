package com.nashmaniac.sunshine;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> weatherList;
    public class DownloadWeatherTask extends AsyncTask<String, Void, String[]>{

        private String getStringRepresentationOfData(long l){
            String DataFormat = "";
            Date dateObject = new Date(l*1000);
            SimpleDateFormat dateFormat= new SimpleDateFormat("MMM dd, yyyy");
            DataFormat = dateFormat.format(dateObject);
            return DataFormat;
        }
        private String getWeatherWithMaxMin(JSONObject j){
            String result = "";
            try {
                JSONArray weatherArray = new JSONArray(j.getString("weather"));
                JSONObject weatherObject = new JSONObject(weatherArray.get(0).toString());
                JSONObject tempObject = new JSONObject(j.getString("temp"));
                Double max = tempObject.getDouble("max");
                Double min = tempObject.getDouble("min");

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                String unit = sharedPreferences.getString(getString(R.string.unit_key), getString(R.string.unit_default));
                if(!unit.equals("metric")){
                    max = ((9*max)/5)+32;
                    min = ((9*min)/5)+32;
                }
                Long maxAmount = Math.round(max);
                Long minAmount = Math.round(min);
                String weather = weatherObject.getString("main");
                result =  weather+" - "+maxAmount+"/"+minAmount;

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return result;
        }
        private String[] getStringSet(String s){
            ArrayList<String> arrayList = new ArrayList<String>();
            try{
                JSONObject jsonObject = new JSONObject(s);
                String listString = jsonObject.getString("list");
                Log.i("listString", listString);

                JSONArray jsonArray = new JSONArray(listString);
                for(int i = 0;i<jsonArray.length();i++){
                    JSONObject partObject = new JSONObject(jsonArray.get(i).toString());
                    long l = partObject.getLong("dt");
                    String DateFormat = getStringRepresentationOfData(l);
                    String weatherTemp = getWeatherWithMaxMin(partObject);
                    Log.v("Date Format", (DateFormat+" - "+weatherTemp));
                    arrayList.add(i, DateFormat+" - "+weatherTemp);
                }
                return arrayList.toArray(new String[arrayList.size()]);
            }catch (Exception e){
                e.printStackTrace();
            }
            return new String[0];
        }
        @Override
        protected String[] doInBackground(String... strings) {
            String result = "";
            URL url;
            HttpURLConnection httpURLConnection = null;

            try{
                url = new URL(strings[0]);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                int data = inputStreamReader.read();
                while(data!=-1){
                    result+=((char) data);
                    data = inputStreamReader.read();
                }
                return getStringSet(result);
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String[] s) {
            super.onPostExecute(s);
            arrayAdapter.clear();
            arrayAdapter.addAll(s);
        }
    }
    public void getWeather(){
        String base_string = "http://api.openweathermap.org/data/2.5/forecast/daily?";
        String api = "04e78386feb51382d49621d4d0460916";
        String api_key = "appid";
        String location_key = "q";
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String location= sharedPreferences.getString(getString(R.string.location_key), getString(R.string.location_default));
        String mode_key = "mode";
        String mode = "json";
        String unit_key="units";
        String unit="metric";
        String count_key="cnt";
        int count = 7;

        Uri uri = Uri.parse(base_string).buildUpon()
                .appendQueryParameter(api_key, api)
                .appendQueryParameter(location_key, location)
                .appendQueryParameter(mode_key, mode)
                .appendQueryParameter(unit_key, unit)
                .appendQueryParameter(count_key, Integer.toString(count))
                .build();
        DownloadWeatherTask downloadWeatherTask = new DownloadWeatherTask();
        downloadWeatherTask.execute(uri.toString());
    }
    public MainActivityFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.listView);

        weatherList = new ArrayList<String>();
        arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item_forecast,R.id.list_itme_forecast_textview, weatherList);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String content = arrayAdapter.getItem(i);
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra("details", content);
                startActivity(intent);
            }
        });
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        getWeather();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.action_refresh){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
