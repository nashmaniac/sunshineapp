package com.nashmaniac.sunshine.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.nashmaniac.sunshine.data.WeatherContract.LocationEntry;
import com.nashmaniac.sunshine.data.WeatherContract.WeatherEntry;
/**
 * Created by nashmaniac on 6/28/16.
 */
public class WeatherDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION=2;
    public static final String DATABASE_NAME = "weather.db";

    public WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String CREATE_LOCATION_TABLE_QUERY ="CREATE TABLE "+ LocationEntry.TABLE_NAME+"("
                +LocationEntry._ID+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
                +LocationEntry.COLUMN_PLACE_NAME+ " TEXT NOT NULL, "
                +LocationEntry.COLUMN_QUERY+ " TEXT UNIQUE NOT NULL, "
                +LocationEntry.COLUMN_LATITUDE+ " REAL NOT NULL, "
                +LocationEntry.COLUMN_LONGITUDE+ " REAL NOT NULL"
                +" )";
        sqLiteDatabase.execSQL(CREATE_LOCATION_TABLE_QUERY);

        final  String CREATE_WEATHER_TABLE_QUERY = "CREATE TABLE "+ WeatherEntry.TABLE_NAME+"("
                +WeatherEntry._ID+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
                +WeatherEntry.COLUMN_LOC_KEY+" INTEGER NOT NULL,"
                +WeatherEntry.COLUMN_DATE+" INTEGER NOT NULL,"
                +WeatherEntry.COLUMN_WEATHER_ID+" INTEGER NOT NULL,"
                +WeatherEntry.COLUMN_DESCRIPTION+" TEXT NOT NULL,"
                +WeatherEntry.COLUMN_MAX_TEMP+" REAL NOT NULL,"
                +WeatherEntry.COLUMN_MIN_TEMP+" REAL NOT NULL,"
                +WeatherEntry.COLUMN_HUMIDITY+" REAL NOT NULL,"
                +WeatherEntry.COLUMN_PRESSURE+" REAL NOT NULL,"
                +" FOREIGN KEY ("+WeatherEntry.COLUMN_LOC_KEY+") REFERENCES "
                +LocationEntry.TABLE_NAME+ "("+LocationEntry._ID+"),"
                +"UNIQUE ("+WeatherEntry.COLUMN_DATE+","+WeatherEntry.COLUMN_LOC_KEY+") ON CONFLICT REPLACE);"
                ;
        sqLiteDatabase.execSQL(CREATE_WEATHER_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+LocationEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+WeatherEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
